package browsers;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import reader.ConfigReader;

public class BrowserFactory 
{
	public static WebDriver driver;
	public ConfigReader reader = new ConfigReader();
	public void getDriver()
	{
		System.setProperty(reader.getDriverProperty(),reader.getDriverPath());
		//driver = new ChromeDriver();
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
