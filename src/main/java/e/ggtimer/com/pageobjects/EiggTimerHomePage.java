package e.ggtimer.com.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class EiggTimerHomePage 
{
	WebDriver driver;
	
	@FindBy(how=How.CSS, using="input[name='start_a_timer']")
	public static WebElement startTimer_txt;
	
	@FindBy(how=How.CSS, using="#timergo")
	public static WebElement go_btn;
	
	@FindBy(how=How.CSS, using="#progressText")
	public static WebElement progressText;
	
	@FindBy(how=How.CSS, using="title")
	public static WebElement title;
	
	public EiggTimerHomePage(WebDriver driver)
	{
		this.driver = driver;
	    PageFactory.initElements(driver, this);
	}
	
	public void setEnterTime(String setTime) 
	{
		startTimer_txt.clear();
		startTimer_txt.sendKeys(setTime);
	}
	
    public void clickGoButton()
    {
    	go_btn.click();
    	
    }
    
    public String getProgressText()
    {
    	return progressText.getText();
    }
}
