package stepdefinitions;

import static org.testng.Assert.assertEquals;
import browsers.BrowserFactory;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import e.ggtimer.com.pageobjects.EiggTimerHomePage;
import reader.ConfigReader;
import runner.EiggTest;

public class Steps extends EiggTest
{
	public EiggTimerHomePage homePageObj;
	public ConfigReader reader = new ConfigReader();
	public BrowserFactory browser = new BrowserFactory();
	public EiggTest eiggObj = new EiggTest();
	
	@Given("^Navigate to Timer Application$")
	public void navigate_to_Timer_Application() throws Throwable 
	{
		browser.getDriver();
		BrowserFactory.driver.get(reader.getApplicationUrl());
	}
	
	@When("^Enter countDown time in the Start a timer textbox$")
	public void enter_countDown_time_in_the_Start_a_timer_textbox() throws Throwable 
	{
		homePageObj = new EiggTimerHomePage(BrowserFactory.driver);
		homePageObj.setEnterTime(reader.getTimeInSecond());
	}
	
	@When("^Click on Go button$")
	public void click_on_Go_button() throws Throwable 
	{
		homePageObj = new EiggTimerHomePage(BrowserFactory.driver);
		homePageObj.clickGoButton();
		Thread.sleep(Integer.parseInt(reader.getTimeInSecond())*1000);
	}
	
	@Then("^Verify the countdown$")
	public void verify_the_countdown() throws Throwable 
	{
		homePageObj = new EiggTimerHomePage(BrowserFactory.driver);
		BrowserFactory.driver.switchTo().alert().accept();
		System.out.println("Timer End Message is: "+homePageObj.getProgressText());
		assertEquals(homePageObj.getProgressText(), "Time Expired!", "Text Match");
		BrowserFactory.driver.close();
	}
}
