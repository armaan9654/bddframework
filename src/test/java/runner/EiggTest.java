package runner;

import org.testng.annotations.Test;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="src//test//resources//features",
					glue={"stepdefinitions"},
					plugin={"pretty","html:target/cucumber-reports/cucumber-pretty"}
				)

@Test
public class EiggTest extends AbstractTestNGCucumberTests{
}
