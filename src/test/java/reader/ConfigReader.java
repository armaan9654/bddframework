package reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader 
{
	private Properties properties;
	private final String propertyFilePath = "src/test/resources/config/Configuration.properties";

	public ConfigReader()
	{
		 BufferedReader reader;
		 try 
		 {
			 reader = new BufferedReader(new FileReader(propertyFilePath));
			 properties = new Properties();
			 try 
			 {
				 properties.load(reader);
				 reader.close();
			 } 
			 catch (IOException e) 
			 {
				 e.printStackTrace();
			 }
		 } 
		 catch (FileNotFoundException e) 
		 {
			 e.printStackTrace();
			 throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		 } 
	}
	
	public String getTimeInSecond() 
	{
		String timeInSecond = properties.getProperty("TIME_SECOND");
		if (timeInSecond != null)
			return timeInSecond;
		else
			throw new RuntimeException("TIME_SECOND not specified in the Configuration.properties file.");
	}

	public String getDriverProperty() 
	{
		String driverProperty = properties.getProperty("WebDriverProperty");
		if (driverProperty != null)
			return driverProperty;
		else
			throw new RuntimeException("WebDriverProperty not specified in the Configuration.properties file.");
	}
	
	public String getDriverPath() 
	{
		String driverPath = properties.getProperty("WebDriverLocation");
		if (driverPath != null)
			return driverPath;
		else
			throw new RuntimeException("WebDriverLocation not specified in the Configuration.properties file.");
	}

	public String getApplicationUrl() 
	{
		String url = properties.getProperty("URL");
		if (url != null)
			return url;
		else
			throw new RuntimeException("URL not specified in the Configuration.properties file.");
	}

}
