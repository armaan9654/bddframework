Feature: Verifying the countdown functionality

 @VerifyCountdownTimeTest
 Scenario: Login to E.ggTimer Application
    Given Navigate to Timer Application
    When Enter countDown time in the Start a timer textbox
    And Click on Go button
    Then Verify the countdown
