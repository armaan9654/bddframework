$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("EiggCounterTimer.feature");
formatter.feature({
  "line": 1,
  "name": "Verifying the countdown functionality",
  "description": "",
  "id": "verifying-the-countdown-functionality",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Login to E.ggTimer Application",
  "description": "",
  "id": "verifying-the-countdown-functionality;login-to-e.ggtimer-application",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@VerifyCountdownTimeTest"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "Navigate to Timer Application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Enter countDown time in the Start a timer textbox",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Click on Go button",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Verify the countdown",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.navigate_to_Timer_Application()"
});
formatter.result({
  "duration": 16789272612,
  "status": "passed"
});
formatter.match({
  "location": "Steps.enter_countDown_time_in_the_Start_a_timer_textbox()"
});
formatter.result({
  "duration": 168561370,
  "status": "passed"
});
formatter.match({
  "location": "Steps.click_on_Go_button()"
});
formatter.result({
  "duration": 33846963265,
  "status": "passed"
});
formatter.match({
  "location": "Steps.verify_the_countdown()"
});
formatter.result({
  "duration": 118369483,
  "status": "passed"
});
});